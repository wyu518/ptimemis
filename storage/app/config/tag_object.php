<?php return array (
  'base' => 
  array (
    'name' => 'tag_object',
    'comment' => '标签与实体关联表',
  ),
  'fields' => 
  array (
    0 => 
    array (
      'name' => 'id',
      'type' => 'int(11)',
      'null' => 'NO',
      'key' => 'PRI',
      'default' => NULL,
      'comment' => '',
      'is_hide' => 0,
    ),
    1 => 
    array (
      'name' => 'tag_id',
      'type' => 'int(11)',
      'null' => 'NO',
      'key' => '',
      'default' => NULL,
      'comment' => '标签id',
      'is_hide' => 0,
    ),
    2 => 
    array (
      'name' => 'object',
      'type' => 'varchar(20)',
      'null' => 'NO',
      'key' => '',
      'default' => NULL,
      'comment' => '实体',
      'is_hide' => 0,
    ),
    3 => 
    array (
      'name' => 'object_id',
      'type' => 'int(11)',
      'null' => 'NO',
      'key' => '',
      'default' => NULL,
      'comment' => '实体id',
      'is_hide' => 0,
    ),
  ),
);