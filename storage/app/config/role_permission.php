<?php return array (
  'base' => 
  array (
    'name' => 'role_permission',
    'comment' => '角色权限关联表',
  ),
  'fields' => 
  array (
    0 => 
    array (
      'name' => 'id',
      'type' => 'int(11)',
      'null' => 'NO',
      'key' => 'PRI',
      'default' => NULL,
      'comment' => '',
      'is_hide' => 0,
    ),
    1 => 
    array (
      'name' => 'role_id',
      'type' => 'int(11)',
      'null' => 'NO',
      'key' => '',
      'default' => NULL,
      'comment' => '角色id',
      'is_hide' => 0,
    ),
    2 => 
    array (
      'name' => 'permission_id',
      'type' => 'int(11)',
      'null' => 'NO',
      'key' => '',
      'default' => NULL,
      'comment' => '权限id',
      'is_hide' => 0,
    ),
  ),
);