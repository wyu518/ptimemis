<?php return array (
  'base' => 
  array (
    'name' => 'user',
    'comment' => '用户表',
  ),
  'fields' => 
  array (
    0 => 
    array (
      'name' => 'id',
      'type' => 'int(11)',
      'null' => 'NO',
      'key' => 'PRI',
      'default' => NULL,
      'comment' => '',
      'is_hide' => 0,
    ),
    1 => 
    array (
      'name' => 'name',
      'type' => 'varchar(20)',
      'null' => 'NO',
      'key' => '',
      'default' => NULL,
      'comment' => '用户名',
      'is_hide' => 0,
    ),
    2 => 
    array (
      'name' => 'password',
      'type' => 'varchar(60)',
      'null' => 'NO',
      'key' => '',
      'default' => NULL,
      'comment' => '密码',
      'is_hide' => 1,
    ),
    3 => 
    array (
      'name' => 'role_id',
      'type' => 'int(11)',
      'null' => 'NO',
      'key' => '',
      'default' => NULL,
      'comment' => '角色id',
      'is_hide' => 0,
    ),
    4 => 
    array (
      'name' => 'created_at',
      'type' => 'timestamp',
      'null' => 'YES',
      'key' => '',
      'default' => 'CURRENT_TIMESTAMP',
      'comment' => '',
      'is_hide' => 0,
    ),
  ),
);