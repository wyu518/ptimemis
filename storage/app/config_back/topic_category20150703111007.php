<?php return array (
  'base' => 
  array (
    'name' => 'topic_category',
    'comment' => '',
  ),
  'fields' => 
  array (
    0 => 
    array (
      'name' => 'id',
      'type' => 'int(11)',
      'null' => 'NO',
      'key' => 'PRI',
      'default' => NULL,
      'comment' => '',
    ),
    1 => 
    array (
      'name' => 'topic_id',
      'type' => 'int(11)',
      'null' => 'NO',
      'key' => '',
      'default' => NULL,
      'comment' => '专题页id',
    ),
    2 => 
    array (
      'name' => 'category_module',
      'type' => 'varchar(20)',
      'null' => 'NO',
      'key' => '',
      'default' => NULL,
      'comment' => '关联分类的模型',
    ),
    3 => 
    array (
      'name' => 'category_id',
      'type' => 'int(11)',
      'null' => 'NO',
      'key' => '',
      'default' => NULL,
      'comment' => '关联分类的id',
    ),
    4 => 
    array (
      'name' => 'config',
      'type' => 'varchar(200)',
      'null' => 'YES',
      'key' => '',
      'default' => NULL,
      'comment' => 'json配置',
    ),
    5 => 
    array (
      'name' => 'weight',
      'type' => 'smallint(6)',
      'null' => 'YES',
      'key' => '',
      'default' => '0',
      'comment' => '排序权重',
    ),
  ),
);