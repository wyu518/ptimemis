<?php return array (
  'base' => 
  array (
    'name' => 'setting',
    'comment' => '系统设置表',
  ),
  'fields' => 
  array (
    0 => 
    array (
      'name' => 'id',
      'type' => 'int(11)',
      'null' => 'NO',
      'key' => 'PRI',
      'default' => NULL,
      'comment' => '',
    ),
    1 => 
    array (
      'name' => 'name',
      'type' => 'varchar(20)',
      'null' => 'NO',
      'key' => '',
      'default' => NULL,
      'comment' => '名称',
    ),
    2 => 
    array (
      'name' => 'key',
      'type' => 'varchar(20)',
      'null' => 'NO',
      'key' => '',
      'default' => NULL,
      'comment' => '键名',
    ),
    3 => 
    array (
      'name' => 'value',
      'type' => 'text',
      'null' => 'NO',
      'key' => '',
      'default' => NULL,
      'comment' => '值',
    ),
  ),
);