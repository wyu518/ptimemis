<?php return array (
  'base' => 
  array (
    'name' => 'link',
    'comment' => '链接表',
  ),
  'fields' => 
  array (
    0 => 
    array (
      'name' => 'id',
      'type' => 'int(11)',
      'null' => 'NO',
      'key' => 'PRI',
      'default' => NULL,
      'comment' => '',
    ),
    1 => 
    array (
      'name' => 'category_id',
      'type' => 'int(11)',
      'null' => 'NO',
      'key' => '',
      'default' => NULL,
      'comment' => '所属分类id',
    ),
    2 => 
    array (
      'name' => 'title',
      'type' => 'varchar(40)',
      'null' => 'NO',
      'key' => '',
      'default' => NULL,
      'comment' => '标题',
    ),
    3 => 
    array (
      'name' => 'description',
      'type' => 'varchar(200)',
      'null' => 'YES',
      'key' => '',
      'default' => NULL,
      'comment' => '描述',
    ),
    4 => 
    array (
      'name' => 'img_dir',
      'type' => 'varchar(200)',
      'null' => 'YES',
      'key' => '',
      'default' => NULL,
      'comment' => '图片地址',
    ),
    5 => 
    array (
      'name' => 'url',
      'type' => 'varchar(200)',
      'null' => 'NO',
      'key' => '',
      'default' => NULL,
      'comment' => '链接地址',
    ),
    6 => 
    array (
      'name' => 'created_at',
      'type' => 'datetime',
      'null' => 'YES',
      'key' => '',
      'default' => 'CURRENT_TIMESTAMP',
      'comment' => '创建时间',
    ),
    7 => 
    array (
      'name' => 'created_by',
      'type' => 'int(11)',
      'null' => 'NO',
      'key' => '',
      'default' => NULL,
      'comment' => '创建用户id',
    ),
    8 => 
    array (
      'name' => 'is_visible',
      'type' => 'tinyint(1)',
      'null' => 'YES',
      'key' => '',
      'default' => '1',
      'comment' => '是否可见',
    ),
    9 => 
    array (
      'name' => 'is_delete',
      'type' => 'tinyint(1)',
      'null' => 'YES',
      'key' => '',
      'default' => '0',
      'comment' => '是否已删除',
    ),
    10 => 
    array (
      'name' => 'weight',
      'type' => 'smallint(6)',
      'null' => 'YES',
      'key' => '',
      'default' => NULL,
      'comment' => '排序权重',
    ),
  ),
);