<?php return array (
  'base' => 
  array (
    'name' => 'category',
    'comment' => '分类表',
  ),
  'fields' => 
  array (
    0 => 
    array (
      'name' => 'id',
      'type' => 'int(11)',
      'null' => 'NO',
      'key' => 'PRI',
      'default' => NULL,
      'comment' => '',
    ),
    1 => 
    array (
      'name' => 'name',
      'type' => 'varchar(20)',
      'null' => 'NO',
      'key' => '',
      'default' => NULL,
      'comment' => '名称',
    ),
    2 => 
    array (
      'name' => 'en_name',
      'type' => 'varchar(40)',
      'null' => 'YES',
      'key' => '',
      'default' => NULL,
      'comment' => '英文名称',
    ),
    3 => 
    array (
      'name' => 'pic_dir',
      'type' => 'varchar(200)',
      'null' => 'YES',
      'key' => '',
      'default' => NULL,
      'comment' => '关联图片地址',
    ),
    4 => 
    array (
      'name' => 'father_id',
      'type' => 'int(11)',
      'null' => 'NO',
      'key' => '',
      'default' => NULL,
      'comment' => '父级分类id',
    ),
    5 => 
    array (
      'name' => 'father_path',
      'type' => 'varchar(500)',
      'null' => 'NO',
      'key' => '',
      'default' => NULL,
      'comment' => '父级分类路径',
    ),
    6 => 
    array (
      'name' => 'module',
      'type' => 'varchar(20)',
      'null' => 'NO',
      'key' => '',
      'default' => NULL,
      'comment' => '应用模型',
    ),
    7 => 
    array (
      'name' => 'weight',
      'type' => 'smallint(6)',
      'null' => 'YES',
      'key' => '',
      'default' => '0',
      'comment' => '排序权重',
    ),
    8 => 
    array (
      'name' => 'is_visible',
      'type' => 'tinyint(1)',
      'null' => 'YES',
      'key' => '',
      'default' => '1',
      'comment' => '是否可见',
    ),
    9 => 
    array (
      'name' => 'is_delete',
      'type' => 'tinyint(1)',
      'null' => 'YES',
      'key' => '',
      'default' => '0',
      'comment' => '是否删除',
    ),
  ),
);