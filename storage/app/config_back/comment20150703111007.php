<?php return array (
  'base' => 
  array (
    'name' => 'comment',
    'comment' => '评论表',
  ),
  'fields' => 
  array (
    0 => 
    array (
      'name' => 'id',
      'type' => 'int(11)',
      'null' => 'NO',
      'key' => 'PRI',
      'default' => NULL,
      'comment' => '',
    ),
    1 => 
    array (
      'name' => 'object',
      'type' => 'varchar(20)',
      'null' => 'NO',
      'key' => '',
      'default' => NULL,
      'comment' => '评论实体',
    ),
    2 => 
    array (
      'name' => 'object_id',
      'type' => 'int(11)',
      'null' => 'NO',
      'key' => '',
      'default' => NULL,
      'comment' => '评论实体id',
    ),
    3 => 
    array (
      'name' => 'father_id',
      'type' => 'int(11)',
      'null' => 'YES',
      'key' => '',
      'default' => '0',
      'comment' => '父级评论id',
    ),
    4 => 
    array (
      'name' => 'content',
      'type' => 'text',
      'null' => 'NO',
      'key' => '',
      'default' => NULL,
      'comment' => '内容',
    ),
    5 => 
    array (
      'name' => 'created_at',
      'type' => 'datetime',
      'null' => 'YES',
      'key' => '',
      'default' => 'CURRENT_TIMESTAMP',
      'comment' => '创建时间',
    ),
    6 => 
    array (
      'name' => 'created_by',
      'type' => 'int(11)',
      'null' => 'YES',
      'key' => '',
      'default' => '0',
      'comment' => '创建用户id',
    ),
    7 => 
    array (
      'name' => 'is_visible',
      'type' => 'tinyint(1)',
      'null' => 'YES',
      'key' => '',
      'default' => '1',
      'comment' => '是否显示',
    ),
    8 => 
    array (
      'name' => 'is_delete',
      'type' => 'tinyint(1)',
      'null' => 'YES',
      'key' => '',
      'default' => '0',
      'comment' => '是否已删除',
    ),
  ),
);