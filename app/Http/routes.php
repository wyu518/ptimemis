<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
//use \DB;
use Illuminate\Http\Response;


$app->get('/', function() {
    return view('index'); 
});

// $app->get('/{table}/config', function($table){
//     $result = DB::select("SHOW FULL COLUMNS FROM $table");
// 	return response()->json($result)->header('Status', 404);
// });

$app->get 	('/{table:[A-Z_a-z]+}/config', 			'ApiController@config'		);

$app->get 	('/{table:[A-Z_a-z]+}', 					'ApiController@index'		);
$app->get 	('/{table:[A-Z_a-z]+}/{id:[0-9]+}', 		'ApiController@show'		);
$app->post 	('/{table:[A-Z_a-z]+}', 					'ApiController@store'		);
$app->put 	('/{table:[A-Z_a-z]+}/{id:[0-9]+}', 		'ApiController@update'		);
$app->delete('/{table:[A-Z_a-z]+}/{id:[0-9]+}', 		'ApiController@destroy'	);

$app->get 	('/config/init', 						'ConfigController@init'		);
